﻿using System;
using System.Collections.Generic;

namespace StringCalculator
{
    public class Calculator
    {
        private readonly string customDelimiterId = "//";
        private readonly string newline = "\n";

        public int Add(string numbers)
        {
            if (string.IsNullOrEmpty(numbers))
            {
                return 0;
            }

            string numberSection = GetNumberSection(numbers);
            string[] delimiters = GetDelimters(numbers);
            List<int> numbersList = GetNumbers(numberSection, delimiters);

            ValidateNumbers(numbersList);

            return GetSum(numbersList);
        }

        private void ValidateNumbers(List<int> numbersList)
        {
            List<string> negativeNumbers = new List<string>();

            foreach (var number in numbersList)
            {
                if (number < 0)
                {
                    negativeNumbers.Add(number.ToString());
                }
            }

            if (negativeNumbers.Count != 0)
            {
                throw new Exception($"Negative not allowed {string.Join(", ", negativeNumbers)}");
            }
        }

        private string[] GetDelimters(string numbers)
        {
            var multipleCustomDelimiterId = $"{customDelimiterId}[";
            var multipleCustomDelimiterSeperator = $"]{newline}";
            var multipleCustomDelimiterSplitter = "][";

            if (numbers.StartsWith(multipleCustomDelimiterId))
            {
                string delimiliters = numbers.Substring(numbers.IndexOf(multipleCustomDelimiterId) + multipleCustomDelimiterId.Length, numbers.IndexOf(multipleCustomDelimiterSeperator) - (multipleCustomDelimiterSeperator.Length + 1));

                return delimiliters.Split(new[] { multipleCustomDelimiterSplitter }, StringSplitOptions.RemoveEmptyEntries);
            }
            else if (numbers.StartsWith(customDelimiterId))
            {
                return new[] { numbers.Substring(numbers.IndexOf(customDelimiterId) + customDelimiterId.Length, numbers.IndexOf(newline) - (newline.Length + 1)) };
            }

            return new[] { ",", newline };
        }

        private string GetNumberSection(string numbers)
        {
            if (numbers.StartsWith(customDelimiterId))
            {
                return numbers.Substring(numbers.IndexOf(newline) + 1);
            }

            return numbers;
        }

        private List<int> GetNumbers(string numbers, string[] delimiters)
        {
            string[] numbersArray = numbers.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
            List<int> numbersList = new List<int>();

            foreach (var num in numbersArray)
            {
                if (int.TryParse(num, out int number))
                {
                    numbersList.Add(number);
                }
            }

            return numbersList;
        }

        private int GetSum(List<int> numbersList)
        {
            int sum = 0;

            foreach (var number in numbersList)
            {
                if (number < 1001)
                {
                    sum += number;
                }
            }

            return sum;
        }
    }
}
