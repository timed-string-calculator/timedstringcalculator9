﻿using NUnit.Framework;
using System;

namespace StringCalculator.Test
{
    [TestFixture]
    public class TestCalculator
    {
        private Calculator _calculator;

        [OneTimeSetUp]
        public void Init()
        {
            _calculator = new Calculator();
        }

        [Test]
        public void GIVEN_EmptyString_WHEN_Adding_THEN_ReturnZero()
        {
            var expected = 0;
            var actual = _calculator.Add(string.Empty);

            Assert.AreEqual(expected, actual);
        }

        [Test]
        public void GIVEN_OneNumber_WHEN_Adding_THEN_ReturnThatNumber()
        {
            var expected = 1;
            var actual = _calculator.Add("1");

            Assert.AreEqual(expected, actual);
        }

        [Test]
        public void GIVEN_TwoNumber_WHEN_Adding_THEN_ReturnSum()
        {
            var expected = 4;
            var actual = _calculator.Add("1,3");

            Assert.AreEqual(expected, actual);
        }

        [Test]
        public void GIVEN_MultipleNumbers_WHEN_Adding_THEN_ReturnSum()
        {
            var expected = 10;
            var actual = _calculator.Add("1,3,6");

            Assert.AreEqual(expected, actual);
        }

        [Test]
        public void GIVEN_NewlineAsADelimiter_WHEN_Adding_THEN_ReturnSum()
        {
            var expected = 10;
            var actual = _calculator.Add("1,3\n6");

            Assert.AreEqual(expected, actual);
        }

        [Test]
        public void GIVEN_SingleCharacterCustomADelimiter_WHEN_Adding_THEN_ReturnSum()
        {
            var expected = 10;
            var actual = _calculator.Add("//;\n1;3;6");

            Assert.AreEqual(expected, actual);
        }

        [Test]
        public void GIVEN_NegativeNumber_WHEN_Adding_THEN_ThrowException()
        {
            var expected = "Negative not allowed -6, -7";
            var actual = Assert.Throws<Exception>(() => _calculator.Add("//;\n1;3;-6;-7"));
            
            Assert.AreEqual(expected, actual.Message);
        }

        [Test]
        public void GIVEN_NumberMoreThanThousand_WHEN_Adding_THEN_IngoreThem()
        {
            var expected = 10;
            var actual = _calculator.Add("//;\n1;3;6;10002");

            Assert.AreEqual(expected, actual);
        }

        [Test]
        public void GIVEN_MultipleCharacterCustomADelimiter_WHEN_Adding_THEN_ReturnSum()
        {
            var expected = 10;
            var actual = _calculator.Add("//;;\n1;;3;;6");

            Assert.AreEqual(expected, actual);
        }

        [Test]
        public void GIVEN_SingleCharacterMultipleCustomADelimiter_WHEN_Adding_THEN_ReturnSum()
        {
            var expected = 10;
            var actual = _calculator.Add("//[*][.]\n1*3.6");

            Assert.AreEqual(expected, actual);
        }

        [Test]
        public void GIVEN_MultipleCharacterMultipleCustomADelimiter_WHEN_Adding_THEN_ReturnSum()
        {
            var expected = 10;
            var actual = _calculator.Add("//[**][..]\n1**3..6");

            Assert.AreEqual(expected, actual);
        }
    }
}
